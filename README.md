# HostinGuard
[![Build Status](https://travis-ci.com/mirkochip/hostinguard.svg?branch=master)](https://travis-ci.com/mirkochip/hostinguard)
[![Code Coverage](coverage.svg)](https://travis-ci.com/mirkochip/hostinguard)

## Requirements
- python 3.6
- python3-venv

## Initial Setup
- Create a virtual environment: `python -m venv venv`
- Activate the virtual environment: ` source venv/bin/activate`
- Install dependencies: ` pip install -r requirements.txt`
- Run it! `python src/manage.py runserver`
