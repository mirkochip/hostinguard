# Application to be monitored definition
# APP
APP1 = 'app1'

# Google API metrics
MEDIUM_DIM = 'rt:medium'
REAL_TIME_USERS = 'rt:activeUsers'
SESSIONS = 'ga:sessions'
UNIQUE_USERS = 'ga:users'
NEW_USERS = 'ga:newUsers'
